<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label> <br>
        <input type="text" id="fname" name="fname" required> <br>
        <label for="lname">Last name:</label> <br>
        <input type="text" id="lname" name="lname" required> <br>

        <br>

        <label for="">Gender:</label> <br>
        <input type="radio" id="mgender">
        <label for="mgender">Male</label> <br>
        <input type="radio" id="fgender">
        <label for="fgender">Female</label> <br>
        <input type="radio" id="ogender">
        <label for="ogender">Other</label> <br><br>

        <label for="">Nationality:</label> <br>
        <select name="" id="">
            <option value="ind">Indonesian</option>
            <option value="sgp">Singaporean</option>
            <option value="mly">Malaysian</option>
            <option value="aus">Australian</option>
        </select> <br><br>

        <label for="">Language Spoken:</label> <br>
        <input type="checkbox" id="ind">
        <label for="ind">Bahasa Indonesia</label> <br>
        <input type="checkbox" id="eng">
        <label for="eng">English</label> <br>
        <input type="checkbox" id="oth">
        <label for="oth" id="oth">Other</label> <br><br>

        <label for="bio">Bio:</label> <br>
        <textarea name="" id="bio" cols="30" rows="10"></textarea> <br><br>

        <button>Sign Up</button>
    </form>
</body>
</html>